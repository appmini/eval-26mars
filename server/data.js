const bcrypt = require('bcrypt');
//
let defaultHashedPwd;

(async () => {
	const saltRounds = 10;
	defaultHashedPwd = await bcrypt.hash('AirFrance', saltRounds);
	console.log('🚀 ~ defaultHashedPwd:', defaultHashedPwd);
})();

//

const players = {
	utilisateurs: [
		{
			id: 1,
			pseudo: 'jean',
			password:
				'$2b$10$AQ7EqbZ7ST4PFT5KSFrNvuYRSB.j1KVkU8a6gE5iLQ0ax/FRPdHIC',
		},
		{
			id: 2,
			pseudo: 'marie',
			password:
				'$2b$10$dt10Ze5e80DJ3G1FzTSUa.APC2XRriwbgkQtvbxPMcP56nig9qvXS',
		},
		{
			id: 3,
			pseudo: 'pierre',
			password:
				'$2b$10$/7ZEEUIALbyXF4qXjfwBZOPLSfF/834sSR73oa5DdrFZj1DRPTAdO',
		},
		{
			id: 4,
			pseudo: 'paul',
			password:
				'$2b$10$/7ZEEUIALbyXF4qXjfwBZOPLSfF/834sSR73oa5DdrFZj1DRPTAdO',
		},
		{
			id: 5,
			pseudo: 'jacques',
			password:
				'$2b$10$MX1M6xigpaQr0AWIoAG2mOapW8/JBbT0dkuCyvgNGmASpQpLuIDsS',
		},
	],
};

module.exports = players;
