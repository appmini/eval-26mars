const express = require('express');
const path = require('path');
let cssPath;
let imgPath;
let jsPath;

const staticRoute = (app) => {
	cssPath = path.join(__dirname, '..', '..', 'public', 'css');
	console.log('🚀 ~ staticRoute ~ cssPath:', cssPath);
	imgPath = path.join(__dirname, '..', '..', 'public', 'img');
	jsPath = path.join(__dirname, '..', '..', 'public', 'js');
	//
	app.use('/css', express.static(cssPath));
	app.use('/img', express.static(imgPath));
	app.use('/js', express.static(jsPath));
};

module.exports = staticRoute;
