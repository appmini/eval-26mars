const http = require('http');
const socketio = require('socket.io');
const app = require('express')();

// Création du serveur HTTP
const server = http.createServer(app);

// Création du serveur Socket.IO
const io = socketio(server);

// Écoute des connexions Socket.IO
io.on('connection', (socket) => {
	console.log(`User ${socket.id.substring(0, 5)} connected`);

	// Upon connection - only to user
	socket.emit('message', 'Welcome to Chat App!');

	// Upon connection - to all others
	socket.broadcast.emit(
		'message',
		`User ${socket.id.substring(0, 5)}} connected`
	);

	// Listening for a message event
	socket.on('message', (data) => {
		console.log(data);
		io.emit('message', `${socket.id.substring(0, 5)}: ${data}`);
	});

	// When user disconnects - to all others
	socket.on('disconnect', () => {
		socket.broadcast.emit(
			'message',
			`User ${socket.id.substring(0, 5)}} disconnected`
		);
	});

	// Listen for activity
	socket.on('activity', (name) => {
		socket.broadcast.emit('activity', name);
	});
});
module.exports = {server, io};
// const {server, io} = require('./socket');
