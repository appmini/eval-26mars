require('dotenv').config();
const ejs = require('ejs');
const {MongoClient} = require('mongodb');
const {Server} = require('socket.io');
const path = require('path');
const {fileURLToPath} = require('url');
const express = require('express');
const http = require('http');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const corsOptions = require('./config/cors-options');
const session = require('express-session');
const router = express.Router();
const bcrypt = require('bcrypt');

const app = express();

const players = require('./data');

//
app.use(
	session({
		name: 'sid',
		secret: 'keyboard cat',
		resave: false,
		saveUninitialized: false,
		cookie: {
			httpOnly: true,
			secure: false,
			maxAge: 1000 * 3600 * 24 * 7,
		},
	})
);
//
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(cors(corsOptions));
//##
app.use(cookieParser());

const jsPath = path.join(__dirname, 'public', 'js');
const cssPath = path.join(__dirname, 'public', 'css');
const imgPath = path.join(__dirname, 'public', 'img');
console.log('🚀 ~ jsPath:', jsPath);
//
app.use('/css', express.static(cssPath));
app.use('/img', express.static(imgPath));
app.use('/js', express.static(jsPath));

app.use(express.urlencoded({extended: true}));
app.use(express.json());

//################ ROUTES AUTHENTIFICATION ###############################
app.get('/', (req, res) => {
	req.session;
	console.log('🚀 ~ ~~~~~~ MIDDLE ACCUEIL   ~~~~~~~~');
	console.log('🚀 ~ app.get ~ req.session:', req.session);

	res.render('index'); // Ajouter la variable dateLisible aux données rendues
});
//
app.get('/log', (req, res) => {
	console.log('🚀 ~ ~~~~~~ MIDDLE LOG   ~~~~~~~~');
	const {name, passwords} = req.body;
	if (name && passwords) {
		const player = players.some((player) => {
			player.name === name;

			if (player) {
				res.render('log', {player});
			}
		});
	} else {
	}

	res.render('log'); // Ajouter la variable dateLisible aux données rendues
});
//
app.get('/reg', (req, res) => {
	console.log('🚀 ~ ~~~~~~ MIDDLE ACCUEIL   ~~~~~~~~');

	res.render('register'); // Ajouter la variable dateLisible aux données rendues
});

app.get('/jeux', (req, res) => {
	console.log('🚀 ~ ~~~~~~ MIDDLE JEUX   ~~~~~~~~');
	if (req.session.user) {
		res.render('jeux');
	} else {
		res.redirect('/log');
	}

	res.render('logout'); // Ajouter la variable dateLisible aux données rendues
});

// app.use('/auth', require('./routes/auth'));
// app.use('/refresh', require('./routes/refresh'));
// app.use('/logout', require('./routes/logout'));
// app.use('/reg', require('./routes/register'));

//######### Vérif token pour routes protégées ######################
// app.use(verifJWT);

//##################################################################
// app.use(errorHandler);

// Définition du répertoire racine pour servir les fichiers statiques
// app.use(express.static(__dirname));

//##################################################################
// app.use(errorHandler);
const PORT = 3080;
const {connectDB} = require('../server/config/_dbConnect');

connectDB();
const mongoose = require('mongoose');

app.listen(PORT, () => {
	console.log(' 🎉  SERVER  : http://localhost:' + PORT);
});
