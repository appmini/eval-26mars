require('dotenv').config();

const {MongoClient} = require('mongodb');

const mongodbUri =
	'mongodb+srv://eli:eli@cluster-ifocop.11zdkja.mongodb.net/?retryWrites=true&w=majority';

const dbName = process.env.DB_NAME;
const colecName = 'player';
//
const client = new MongoClient(mongodbUri);

//####################################################
// const dbURI = process.env.DB_URI;
// const DB_NAME = process.env.DB_NAME;
// const DB_COLEC = process.env.DB_COLEC;

const connectDB = () => {
	return client
		.connect()
		.then(() => client.db(dbName))
		.then(() => client.db(dbName).command({ping: 1}))
		.then((result) => {
			console.log('🚀 ~ result:', result);
			console.log(`Ping à la base de données réussi : ${result.ok}`);
		})
		.then(() => client.db(dbName).createCollection(colecName))
		.then(() => {
			console.log(
				`Base de données ${dbName} et collection ${colecName} créée avec succès.`
			);
		})
		.finally(() => client.close());
};

module.exports = {connectDB};
